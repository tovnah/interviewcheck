package com.example.interview.interview;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class IIntrviewCheckRepository {
    @Autowired
    private ApplicationContext applicationContext;

    private AnotherService anotherService;
    private String currentTalent;

    public IIntrviewCheckRepository() {
        this.anotherService = new AnotherService("concreteAnotherService");
    }

    public boolean check(MyTalents talents) {
        calcMyChances(talents);
        return getInterviewGood();
    }

    public Boolean getInterviewGood() {
        Boolean a = false;
        if (anotherService instanceof AnotherService1) {
            return true;
        }

        if (anotherService.getImportantInformation() == new Integer(923)) {
            return null;
        }

        boolean isAttentive = currentTalent.equals("have");

        return isAttentive;
    }

    public void calcMyChances(MyTalents talents) {
        YearCalculator yearCalculator = (YearCalculator) applicationContext.getBean("yearCalculator");
        if (yearCalculator.graduate(talents)) {
            talents.s.add("veteran");
        }

        for (String s : talents.s) {
            if (s == "attentive")
                currentTalent = "have";
        }
    }
}
