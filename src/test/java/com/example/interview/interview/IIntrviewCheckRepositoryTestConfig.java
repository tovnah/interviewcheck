package com.example.interview.interview;

import org.springframework.context.annotation.Bean;

public class IIntrviewCheckRepositoryTestConfig {
    @Bean
    public IIntrviewCheckRepository iIntrviewCheckRepository() {
        return new IIntrviewCheckRepository();
    }

    @Bean
    public YearCalculator yearCalculator() {
        return new YearCalculator();
    }
}
