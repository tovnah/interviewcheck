package com.example.interview.interview;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ContextConfiguration(classes = IIntrviewCheckRepositoryTestConfig.class)
class IIntrviewCheckRepositoryTest {

    @Test
    void should_work() {
        MyTalents talents = new MyTalents();
        talents.s = new ArrayList<>();
        talents.s.add("attentive");

        assertTrue(iIntrviewCheckRepository.check(talents));
    }

    @Autowired
    public IIntrviewCheckRepository iIntrviewCheckRepository;
}